<?php

namespace OC\TestAnnotationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/annot", name="annotation")
     */
    public function indexAction()
    {
        return $this->render('OCTestAnnotationBundle:Default:index.html.twig');
    }
}
