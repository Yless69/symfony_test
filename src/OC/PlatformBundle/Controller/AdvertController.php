<?php

namespace OC\PlatformBundle\Controller;

use OC\PlatformBundle\Entity\Advert;
use OC\PlatformBundle\Form\AdvertEditType;
use OC\PlatformBundle\Form\AdvertType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
// N'oubliez pas ce use pour l'annotation
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Description of AdvertController
 *
 * @author ylesseux
 */
class AdvertController extends Controller {

    // ////////////////////////////////////////////////
    // Méthodes majeures
    // ////////////////////////////////////////////////
    public function indexAction($page) {
        if ($page < 1) {
            throw new NotFoundHttpException('Page "' . $page . '" inexistante.');
        }

        // Ici je fixe le nombre d'annonces par page à 3
        // Mais bien sûr il faudrait utiliser un paramètre, et y accéder via $this->container->getParameter('nb_per_page')
        $nbPerPage = 3;

        // On récupère notre objet Paginator
        $listAdverts = $this->getDoctrine()
                ->getManager()
                ->getRepository('OCPlatformBundle:Advert')
                ->getAdverts($page, $nbPerPage)
        ;

        // On calcule le nombre total de pages grâce au count($listAdverts) qui retourne le nombre total d'annonces
        $nbPages = ceil(count($listAdverts) / $nbPerPage);

        // Si la page n'existe pas, on retourne une 404
        if ($page > $nbPages) {
            throw $this->createNotFoundException("La page " . $page . " n'existe pas.");
        }

        // On donne toutes les informations nécessaires à la vue
        return $this->render('OCPlatformBundle:Advert:index.html.twig', array(
                    'listAdverts' => $listAdverts,
                    'nbPages' => $nbPages,
                    'page' => $page,
        ));
    }

    public function viewAction($id) {
        $em = $this->getDoctrine()->getManager();

        // Pour récupérer une seule annonce, on utilise la méthode find($id)
        $advert = $em->getRepository('OCPlatformBundle:Advert')->find($id);

        // $advert est donc une instance de OC\PlatformBundle\Entity\Advert
        // ou null si l'id $id n'existe pas, d'où ce if :
        if (null === $advert) {
            throw new NotFoundHttpException("L'annonce d'id " . $id . " n'existe pas.");
        }

        // Récupération de la liste des candidatures de l'annonce
        $listApplications = $em
                ->getRepository('OCPlatformBundle:Application')
                ->findBy(array('advert' => $advert))
        ;

        // Récupération des AdvertSkill de l'annonce
        $listAdvertSkills = $em
                ->getRepository('OCPlatformBundle:AdvertSkill')
                ->findBy(array('advert' => $advert))
        ;

        return $this->render('OCPlatformBundle:Advert:view.html.twig', array(
                    'advert' => $advert,
                    'listApplications' => $listApplications,
                    'listAdvertSkills' => $listAdvertSkills,
        ));
    }

    /**
     * @Security("has_role('ROLE_AUTEUR')")
     */
    public function addAction(Request $request) {
        // On crée un objet Advert
        $advert = new Advert();

        // J'ai raccourci cette partie, car c'est plus rapide à écrire !
        $form = $this->createForm(AdvertType::class, $advert);

        // Si la requête est en POST
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {

            // On enregistre notre objet $advert dans la base de données, par exemple
            $em = $this->getDoctrine()->getManager();
            $em->persist($advert);
            $em->flush();

            $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

            // On redirige vers la page de visualisation de l'annonce nouvellement créée
            return $this->redirectToRoute('oc_platform_view', array('id' => $advert->getId()));
        }


        // À ce stade, le formulaire n'est pas valide car :
        // - Soit la requête est de type GET, donc le visiteur vient d'arriver sur la page et veut voir le formulaire
        // - Soit la requête est de type POST, mais le formulaire contient des valeurs invalides, donc on l'affiche de nouveau
        return $this->render('OCPlatformBundle:Advert:add.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $advert = $em->getRepository('OCPlatformBundle:Advert')->find($id);

        if (null === $advert) {
            throw new NotFoundHttpException("L'annonce d'id " . $id . " n'existe pas.");
        }

        // J'ai raccourci cette partie, car c'est plus rapide à écrire !
        $form = $this->createForm(AdvertEditType::class, $advert);

        // Si la requête est en POST
        if ($request->isMethod('POST')) {
            // On fait le lien Requête <-> Formulaire
            // À partir de maintenant, la variable $advert contient les valeurs entrées dans le formulaire par le visiteur
            $form->handleRequest($request);

            // On vérifie que les valeurs entrées sont correctes
            // (Nous verrons la validation des objets en détail dans le prochain chapitre)
            if ($form->isValid()) {
                // On enregistre notre objet $advert dans la base de données, par exemple
                $em = $this->getDoctrine()->getManager();
                $em->persist($advert);
                $em->flush();

                $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');

                // On redirige vers la page de visualisation de l'annonce nouvellement créée
                return $this->redirectToRoute('oc_platform_view', array('id' => $advert->getId()));
            }
        }

        // À ce stade, le formulaire n'est pas valide car :
        // - Soit la requête est de type GET, donc le visiteur vient d'arriver sur la page et veut voir le formulaire
        // - Soit la requête est de type POST, mais le formulaire contient des valeurs invalides, donc on l'affiche de nouveau
        return $this->render('OCPlatformBundle:Advert:edit.html.twig', array(
                    'advert' => $advert,
                    'form' => $form->createView(),
        ));
    }

    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $advert = $em->getRepository('OCPlatformBundle:Advert')->find($id);

        if (null === $advert) {
            throw new NotFoundHttpException("L'annonce d'id " . $id . " n'existe pas.");
        }

        // On crée un formulaire vide, qui ne contiendra que le champ CSRF
        // Cela permet de protéger la suppression d'annonce contre cette faille
        $form = $this->get('form.factory')->create();

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->remove($advert);
            $em->flush();

            $request->getSession()->getFlashBag()->add('info', "L'annonce a bien été supprimée.");

            return $this->redirectToRoute('oc_platform_home');
        }

        return $this->render('OCPlatformBundle:Advert:delete.html.twig', array(
                    'advert' => $advert,
                    'form' => $form->createView(),
        ));
    }

    public function menuAction($limit) {
        $em = $this->getDoctrine()->getManager();

        $listAdverts = $em->getRepository('OCPlatformBundle:Advert')->findBy(
                array(), // Pas de critère
                array('date' => 'desc'), // On trie par date décroissante
                $limit, // On sélectionne $limit annonces
                0                        // À partir du premier
        );

        return $this->render('OCPlatformBundle:Advert:menu.html.twig', array(
                    'listAdverts' => $listAdverts
        ));
    }

// ////////////////////////////////////////////////
// Méthodes de test
// ////////////////////////////////////////////////
    public function urlAction($page) {
// return new Response('Notre propre "Hello World !"');
        $url = $this->generateUrl('oc_platform_view', array("id" => 5), UrlGeneratorInterface::ABSOLUTE_URL);
        $content = $this->get('templating')->render(
                'OCPlatformBundle:Advert:url.html.twig', array(
            "nom" => "Yannou",
            "url" => $url,
            "page" => $page
        ));
        return new Response($content);
    }

    public function sessionAction($id, Request $request) {
// Récupération de la session
        $session = $request->getSession();
// On définit une nouvelle valeur pour cette variable user_id
        $session->set('user_id', 91);
// On récupère le contenu de la variable user_id
        $userId = $session->get('user_id');

// Exemple de Construction d'une réponse
        $response = new Response();
// On définit le contenu
        $response->setContent("Ceci est une page d'erreur 404");
// On définit le code HTTP à « Not Found » (erreur 404)
        $response->setStatusCode(Response::HTTP_NOT_FOUND);
//__________________________________
// Exemple de récuppération d'un paramètre GET
        $tag = $request->query->get("tag");
        $str = "Affichage de l'annonce d'id : " . $id . ", avec le tag : " . $tag;
//__________________________________

        $rendu = $this->render('OCPlatformBundle:Advert:session.html.twig', array(
            'id' => $id,
            'tag' => $tag,
            'userId' => $userId
        ));
        return $rendu;
    }

    public function flashAction(Request $request) {

        $session = $request->getSession();
// Bien sûr, cette méthode devra réellement ajouter l'annonce
// Mais faisons comme si c'était le cas
        $session->getFlashBag()->add('info', 'Annonce bien enregistrée');
// Le « flashBag » est ce qui contient les messages flash dans la session
// Il peut bien sûr contenir plusieurs messages :
        $session->getFlashBag()->add('info', 'Oui oui, elle est bien enregistrée !');
// Puis on redirige vers la page de visualisation de cette annonce
        return $this->redirectToRoute('oc_platform_view', array('id' => 5));
    }

    public function jsonAction($id) {
// Créons nous-mêmes la réponse en JSON, grâce à la fonction json_encode()
        $response = new Response(json_encode(array('id' => $id)));

// Ici, nous définissons le Content-type pour dire au navigateur
// que l'on renvoie du JSON et non du HTML
        $response->headers->set('Content-Type', 'application/json');

// return $response;
// Ou le raccourcit suivant :
        return new JsonResponse(array('id' => $id));
    }

    public function redirAction() {
        return $this->redirectToRoute('oc_platform_home');
    }

    public function testAction() {

        $repository = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository('OCPlatformBundle:Advert')
        ;

        $listAdverts = $repository->myFindAll();
        return $this->render('OCPlatformBundle:Advert:index.html.twig', array('listAdverts' => $listAdverts));
    }

    public function helloAction() {
// return new Response('Notre propre "Hello World !"');
        $content = $this->get('templating')->render('OCPlatformBundle:Advert:hello.html.twig', array("nom" => "Yannou"));
        return new Response($content);
    }

    public function exitAction() {
// return new Response('Notre propre "Hello World !"');
        $content = $this->get('templating')->render('OCPlatformBundle:Advert:exit.html.twig', array("nom" => "Lesseux", "prenom" => "Yann"));
        return new Response($content);
    }

    public function listAction() {
        $listAdverts = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository('OCPlatformBundle:Advert')
                ->getAdvertWithApplications()
        ;

        foreach ($listAdverts as $advert) {
            // Ne déclenche pas de requête : les candidatures sont déjà chargées !
            // Vous pourriez faire une boucle dessus pour les afficher toutes
            $advert->getApplications();
        }

// …
    }

}
